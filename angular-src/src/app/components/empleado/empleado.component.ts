import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmpleadoService } from 'src/app/services/empleado.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {
  empleadoPost: FormGroup;
  empleadoEdit: FormGroup;
  base64: any;
  imgEdit: any ;
  display: any = 'none';
  empleados: any[] = [];
  p: number = 1;

  constructor(public fb: FormBuilder, public empleadoService: EmpleadoService) { }

  ngOnInit() {
    this.empleadoPost = this.fb.group({
      brm: ['', Validators.required],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      imagen: ['', Validators.required],
      base64: ['']
    })
    this.empleadoEdit = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      imagen: [''],
      base64: ['']
    })
    this.bindTable()
  }

  bindTable() {
    this.empleadoService.getAllEmployee().subscribe((res) => {
      this.empleados = res;
    },
      (error) => {
        console.error(error);
      });
  }


  eliminar(empleado: any) {
    Swal.fire({
      title: '¿Esta seguro?',
      text: 'se eliminara el empleado con BRM ' + empleado.brm,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: "#d33"
    }).then((result) => {
      if (result.value) {
        this.empleadoService.delete(empleado.id).subscribe((res) => {
          Swal.fire('empleado eliminado')
          this.empleados = this.empleados.filter(e => e.id != empleado.id);
        },
          (error) => {
            console.error(error);
          });
      }
    });
  }

  guardar() {
    var element = <HTMLInputElement>document.getElementById("saveButton");
    element.disabled = true;
    var temporal = this.empleadoPost;
    temporal.value.imagen = temporal.value.imagen.split(/(\\|\/)/g).pop()
    temporal.value.base64 = this.base64
    this.empleadoPost = temporal;
    this.empleadoService.post(this.empleadoPost.value).subscribe(
      resp => {
        this.empleadoPost.reset();
        Swal.fire(
          'Empleado creado',
          'Empleado con BRM: ' + resp.brm + ' creado',
          'success'
        )
        this.empleados.push(resp)
      },
      error => { 
        Swal.fire(
          'Error',
          'El BRM: ' + this.empleadoPost.value.brm + ' ya se encuentra en uso',
          'error'
        )
        element.disabled = false;
       }
    )
  }

  actualizar() {
    var element = <HTMLInputElement>document.getElementById("btnActualizar");
    element.disabled = true;
    var temporal = this.empleadoEdit;
    (temporal.value.imagen != '') ? temporal.value.imagen = temporal.value.imagen.split(/(\\|\/)/g).pop() : temporal.value.imagen = this.nameImg
    temporal.value.base64 = this.imgEdit
    this.empleadoEdit = temporal;
    this.empleadoService.put(this.empleadoEdit.value, this.empleadoEdit.value.id).subscribe(
      resp => {
        this.empleadoEdit.reset();
        Swal.fire(
          'Empleado actualizado',
          'Empleado con BRM: ' + resp.brm + ' actualizado',
          'success'
        )
        this.onCloseHandled();
        element.disabled = false;
        this.empleados = this.empleados.filter(e => e.id != resp.id);
        this.empleados.push(resp)
      },
      error => { console.error(error) }
    )
  }

  actionMethod(event: any) {
    event.target.disabled = true;
  }

  nameImg: string;
  openModal(empleado) {
    this.empleadoEdit.setValue({
      id: empleado.id,
      nombre: empleado.nombre,
      puesto: empleado.puesto,
      imagen: '',
      base64: empleado.base64
    })
    this.nameImg = empleado.imagen
    this.imgEdit = empleado.base64;
    this.display = 'block';
  }

  onCloseHandled() {
    this.display = 'none';
  }


  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64 = reader.result.toString().replace('data:image/jpeg;base64,', '');
    };
  }

  handleUploadEdit(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgEdit = reader.result.toString().replace('data:image/jpeg;base64,', '');
    };
  }
}
