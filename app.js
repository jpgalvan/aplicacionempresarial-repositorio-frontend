const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');



const app = express();

const empleado  = require('./routes/empleado');

//const port = 3000;
const port = process.env.PORT || 8080;
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json({ limit: "50mb" }))
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))


app.use('/empleado', empleado);


app.get('/', (req, res) => {
    res.send('Invalid endpoint');
});

app.listen(port, () => {
    console.log("Server startet on port: " + port)
});