const express = require('express');
const res = require('express/lib/response');
const router = express.Router();
const axios = require('axios');

const BASE_URL = 'https://aplicacionempresarial-api.herokuapp.com/empleado/';



router.get('/', (req,res) => {
    axios({
        method: 'get',
        url: BASE_URL
    }).then((response) => { 
        res.status(200).send(response.data)
    }, (err) => {
        res.status(404).send('err');
    });
});

router.get('/:id', (req, res) => {
    axios({
        method: 'get',
        url: BASE_URL + req.params.id
    }).then((response) => { 
        res.status(200).send(response.data)
    }, (err) => {
        res.status(404).send('err');
    });
});

router.delete('/:id', (req, res) => {
    axios({
        method: 'delete',
        url: BASE_URL + req.params.id
    }).then((response) => { 
        res.status(200).send(response.data)
    }, (err) => {
        res.status(404).send('err');
    });
});

router.post('/', (req, res) => {
    axios({
        method: 'post',
        url: BASE_URL,
        data: req.body
    }).then((response) => { 
        res.status(201).send(response.data)
    }, (err) => {
        res.status(400).send('err');
    });
});


router.put('/:id', (req, res) => {
    axios({
        method: 'put',
        url: BASE_URL + req.params.id,
        data: req.body
    }).then((response) => { 
        res.status(200).send(response.data)
    }, (err) => {
        res.status(400).send('err');
    });
});

module.exports = router;